var connection = require('../config/conn');

exports.addjobschedule = function(req, res) {
    console.log(req.body);
    var clientid = req.body.clientid
    var campaignid = req.body.campaignid;
    var jobdate = req.body.jobdate;
    var caption = req.body.caption;
    var mediatype = req.body.mediatype;
    var mediaurl = req.body.mediaurl;
    var categorytarget = req.body.categorytarget;
    var influencers = req.body.reqinfluencers;
    var reportimage = req.body.reportimage;
    var reportlink = req.body.reportlink;
    var status = 0;
    var substatus = "jobcreated";
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO jobschedule (client_id,campaign_id,created_date, modified_date, status, job_date, caption,media_type,media_url,category_target) values (?,?,?,?,?,?,?,?,?,?);', [clientid, campaignid, created_date, modified_date, status, jobdate, caption, mediatype, mediaurl, categorytarget],
        function(error, result) {
            if (error) {
                res.status(404).json(error);
            }

        }).then(
        influencers.forEach(function(ele, i) {
            connection.query('INSERT INTO user_jobschedule (user_influencer_id,job_schedule_id,status, created_date, modified_date, report_image_url, report_link,client_id) values (?,LAST_INSERT_ID(),?,?,?,?,?,?);', [ele, status, created_date, modified_date, reportimage, reportlink, clientid],
                function(error) {

                    if (error) {
                        res.status(404).json(error);

                    }
                })
        }),
        connection.query('UPDATE campaigns SET substatus=?, modified_date=? WHERE id = ?', [substatus, modified_date, campaignid],
            function(error) {

                if (error) {
                    res.status(404).json(error);
                } else {
                    res.status(200).json("success");
                }
            })

    );

};
exports.getalljobschedules = (req, res, next) => {

    connection.query('SELECT * FROM jobschedule',
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.getalluserjobschedules = (req, res, next) => {

    connection.query('SELECT * FROM user_jobschedule',
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.getjobschedule = (req, res, next) => {
    console.log(req.params.id);
    var id = req.params.id;

    connection.query('SELECT * FROM jobschedule where client_id = ?', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.getinfluencerjobschedule = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM jobschedule where id = (SELECT job_schedule_id FROM user_jobschedule where user_influencer_id=?)', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getinfluenceruserjobschedule = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM user_jobschedule where user_influencer_id = ?', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.influencerapprove = (req, res) => {
    var id = req.body.id;
    var jobid = req.body.jobid;
    var modified_date = new Date();
    connection.query('UPDATE user_jobschedule SET status=?, modified_date=? WHERE id = ?', [2, modified_date, id],
        function(error) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json("success");
            }
        });

};
exports.influencerreject = (req, res) => {
    var id = req.body.id;
    var jobid = req.body.jobid;
    var modified_date = new Date();
    connection.query('UPDATE user_jobschedule SET status=?, modified_date=? WHERE id = ?', [5, modified_date, id],
        function(error) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json("success");
            }
        });

};