var connection = require('../config/conn');

exports.addcampaign = function(req, res) {
    console.log(req.body);
    var clientid = req.body.clientid;
    var product_name = req.body.productname;
    var product_category = req.body.productcategory;
    var product_description = req.body.productdescription;
    var promotion_goal = req.body.promotiongoal;
    var promotion_budget = req.body.promotionbudget;
    var promotion_description = req.body.promotiondescription;
    var status = 1;
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO campaigns (user_client_id,product_name, product_category_id, product_description, promotion_goal_id, promotion_budget,promotion_description,status,created_date, modified_date) values (?,?,?,?,?,?,?,?,?,?);', [clientid, product_name, product_category, product_description, promotion_goal, promotion_budget, promotion_description, status, created_date, modified_date],
        function(error) {
            if (error) {
                res.status(404).json(error);
            }

            res.status(200).json("success");
        })
};
exports.getclientcampaign = (req, res, next) => {
    console.log(req.params.id);
    var id = req.params.id;

    connection.query('SELECT * FROM campaigns where user_client_id = ?', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.getcampaignbyid = (req, res, next) => {
    console.log(req.params.id);
    var id = req.params.id;

    connection.query('SELECT * FROM campaigns where id= ?', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.getallcampaigns = (req, res, next) => {
    connection.query('SELECT campaigns.*, user_client_details.first_name FROM campaigns LEFT JOIN user_client_details ON (user_client_details.user_id=campaigns.user_client_id)',
        function(error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.adminapprove = (req, res, next) => {
    var id = req.params.id;
    var modified_date = new Date();
    connection.query('UPDATE campaigns SET status=?, modified_date=? WHERE id = ?', [2, modified_date, id],
        function(error) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json("success");
            }
        })

};
exports.adminreject = (req, res, next) => {
    var id = req.params.id;
    var modified_date = new Date();
    connection.query('UPDATE campaigns SET status=?, modified_date=? WHERE id = ?', [9, modified_date, id],
        function(error) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json("success");
            }
        });
};

exports.getinfluencercampaign = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM campaigns where user_client_id = (SELECT client_id FROM user_jobschedule where user_influencer_id=?)', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.updatecampaignstatus = (req, res) => {
    var id = req.body.id;
    var status = req.body.status;
    var modified_date = new Date();
    connection.query('UPDATE campaigns SET status=?, modified_date=? WHERE id = ?', [status, modified_date, id],
        function(error) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json("success");
            }
        });
};