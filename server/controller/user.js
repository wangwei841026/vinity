var connection = require('../config/conn');
var bcrypt = require('bcrypt');
exports.addclient = function(req, res) {
    console.log(req.body);
    var first_name = req.body.firstname;
    var last_name = req.body.lastname
    var email = req.body.email;
    var password = bcrypt.hashSync(req.body.password, 10);
    var user_type_id = 1;
    var active = 1;
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO users (email, password, active, user_type_id, created_date, modified_date) values (?,?,?,?,?,?);', [email, password, active, user_type_id, created_date, modified_date],
        function(error) {
            if (error)
                res.status(404).json(error);

        }).then(connection.query('INSERT INTO user_client_details (user_id, first_name, last_name, created_date, modified_date) values (LAST_INSERT_ID(),?,?,?,?);', [first_name, last_name, created_date, modified_date],
        function(error) {
            if (error) {
                res.status(404).json(error);
                return;
            } else {
                res.status(200).json("success");
            }
        }));
};
exports.addinfluencer = function(req, res) {
    console.log(req.body);
    var first_name = req.body.firstname;
    var last_name = req.body.lastname
    var email = req.body.email;
    var password = bcrypt.hashSync(req.body.password, 10);
    var user_type_id = 2;
    var active = 1;
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO users (email, password, active, user_type_id, created_date, modified_date) values (?,?,?,?,?,?);', [email, password, active, user_type_id, created_date, modified_date],
        function(error, rows, fields) {
            if (error)
                res.status(404).json(err);

        }).then(connection.query('INSERT INTO user_influencer_details (user_id, first_name, last_name, created_date, modified_date) values (LAST_INSERT_ID(),?,?,?,?);', [first_name, last_name, created_date, modified_date],
        function(error, rows, fields) {
            if (error) {
                res.status(404).json(err);
                return;
            } else {
                res.status(200).json("success");
            }
        }));
};
exports.login = (req, res, next) => {
    var email = req.body.email;
    var password = req.body.password;

    connection.query('SELECT id,email,password,active,user_type_id,created_date,modified_date FROM users where email = ? ', [email],
        function(error, rows, fields) {
            if (error) {
                res.status(404).json(err);
                return;
            }
            if (rows.length) {
                if (bcrypt.compareSync(password, rows[0]['password'])) {
                    res.json({
                        "success": true,
                        "user": rows
                    });
                } else {
                    res.json({
                        "success": false,
                        "message": "Passwords did not match"
                    });
                }
            } else {
                res.json({
                    "success": false,
                    "message": "User not found"
                });
            }
        });
};

exports.getadmin = (req, res, next) => {
    console.log(req.params.id);
    var id = req.params.id;

    connection.query('SELECT id,email,active,user_type_id,created_date,modified_date FROM users where id = ?', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.updateadmin = (req, res, next) => {
    var id = req.body.id;
    var email = req.body.email;
    var modified_date = new Date();

    connection.query('UPDATE users SET email = ?, modified_date=?  WHERE id = ?', [email, modified_date, id],
        function(error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json("Success");

            }
        });
};
exports.updateuserpass = (req, res, next) => {
    var id = req.body.id;
    var pass = bcrypt.hashSync(req.body.password, 10);
    var modified_date = new Date();

    connection.query('UPDATE users SET password=?, modified_date=? WHERE id = ?', [pass, modified_date, id],
        function(error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json("Success");

            }
        });
};
exports.getclient = (req, res, next) => {

    var id = req.params.id;
    console.log(id);
    connection.query('SELECT  user_client_details.*, users.`email` FROM user_client_details LEFT JOIN users ON(users.id = user_client_details.user_id)  WHERE users.`id` =? ; ', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json(rows);
            }
        });
};
exports.updateclient = function(req, res) {
    console.log(req.body);
    var id = req.body.id;
    var user_id = req.body.user_id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name
    var email = req.body.email;
    var phone = req.body.phone;
    var company_address = req.body.company_address;
    var company_description = req.body.company_description;
    var company_name = req.body.company_name;
    var company_photo = req.body.company_photo;
    var company_website = req.body.company_website;
    var modified_date = new Date();
    connection.query('UPDATE users SET email=?, modified_date=? WHERE id = ?', [email, modified_date, user_id],
        function(error, rows, fields) {
            if (error)
                res.status(404).json(error);
            return;

        }).then(connection.query('UPDATE user_client_details SET first_name=?, last_name=?, phone=?,email=?,company_name=?,company_description=?, company_website=?, company_photo=?, company_address=?,modified_date=? WHERE id = ?', [first_name, last_name, phone, email, company_name, company_description, company_website, company_photo, company_address, modified_date, id],
        function(error, rows, fields) {

            if (error) {
                res.status(404).json(error);

            } else {
                res.status(200).json("success");
            }
        }));
};
exports.getinfluencer = (req, res, next) => {

    var id = req.params.id;
    console.log(id);
    connection.query('SELECT  user_influencer_details.*, users.`email` FROM user_influencer_details LEFT JOIN users ON(users.id = user_influencer_details.user_id)  WHERE users.`id` =? ; ', [id],
        function(error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json(rows);
            }
        });
};

exports.updateinfluencer = function(req, res) {
    console.log(req.body);
    var id = req.body.id;
    var user_id = req.body.user_id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name
    var email = req.body.email;
    var phone = req.body.phone;
    var address = req.body.address;
    var address_note = req.body.address_note;
    var avatar = req.body.avatar;
    var bank_account_name = req.body.bank_account_name;
    var bank_account_number = req.body.bank_account_number;
    var bank_branch = req.body.bank_branch;
    var bank_name = req.body.bank_name;
    var biography = req.body.biography;
    var birthday = req.body.date_of_birth;
    var birth = birthday.substr(0, 19);
    var gender = req.body.gender;
    var modified_date = new Date();
    connection.query('UPDATE users SET email=?, modified_date=? WHERE id = ?', [email, modified_date, user_id],
        function(error, rows, fields) {
            if (error)
                res.status(404).json(error);
            return;

        }).then(connection.query('UPDATE user_influencer_details SET first_name=?, last_name=?, phone=?,address=?,address_note=?, avatar=?, bank_account_name=?, bank_account_number=?,bank_branch=?,bank_name=?,biography=?,date_of_birth=?,gender=?,modified_date=? WHERE id = ?', [first_name, last_name, phone, address, address_note, avatar, bank_account_name, bank_account_number, bank_branch, bank_name, biography, birth, gender, modified_date, id],
        function(error, rows, fields) {

            if (error) {
                res.status(404).json(error);

            } else {
                res.status(200).json("success");
            }
        }));
};
exports.getallinfluencers = (req, res) => {

    connection.query('SELECT  * FROM user_influencer_details',
        function(error, rows) {
            if (error) {
                console.log(error)
            } else {
                res.status(200).json(rows);
            }
        });
};