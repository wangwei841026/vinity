var connection = require('../config/conn');

exports.addinvoice = function(req, res) {
    console.log(req.body);
    var clientid = req.body.clientid;
    var campaignid = req.body.campaignid;
    var amount = req.body.amount;
    var status = 0;
    var substatus = "invoicecreated"
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO invoice (user_client_id,campaign_id, status, created_date, modified_date,amount) values (?,?,?,?,?,?);', [clientid, campaignid, status, created_date, modified_date, amount],
        function(error) {
            if (error)
                res.status(404).json(error);

        }).then(connection.query('UPDATE campaigns SET substatus=?, modified_date=? WHERE id = ?', [substatus, modified_date, campaignid],
        function(error) {
            if (error) {
                res.status(404).json(error);
                return;
            } else {
                res.status(200).json("success");
            }
        }));
};
exports.getinvoice = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM invoice where user_client_id = ?', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.addpayment = function(req, res) {
    console.log(req.body);

    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO payment (user_client_id,campaign_id, status, created_date, modified_date,amount) values (?,?,?,?,?,?);', [clientid, campaignid, status, created_date, modified_date, amount],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getjobscheduleid = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT id FROM jobschedule WHERE campaign_id = ? ', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getinfluencerid = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT user_influencer_id FROM user_jobschedule WHERE job_schedule_id = ? ', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getsocialmediaid = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT socialmedia_id FROM jobschedule_socialmedia WHERE jobschedule_id = ? ', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getamount = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT price FROM influencer_rate WHERE social_media_id = ? ', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.addpayment = function(req, res) {
    console.log(req.body);
    var campaignid = req.body.campaignid;
    var influencerid = req.body.influencerid;
    var jobscheduleid = req.body.jobscheduleid;
    var amount = req.body.amount;
    var status = 0;
    var created_date = new Date();
    var modified_date = new Date();
    connection.query('INSERT INTO payment (user_influencer_id,job_schedule_id, status, created_date, modified_date,amount) values (?,?,?,?,?,?);', [influencerid, jobscheduleid, status, created_date, modified_date, amount],
        function(error) {
            if (error)
                res.status(404).json(error);

        }).then(connection.query('UPDATE campaigns SET status=?, modified_date=? WHERE id = ?', [7, modified_date, campaignid],
        function(error) {
            if (error) {
                res.status(404).json(error);
                return;
            } else {
                res.status(200).json("success");
            }
        }));
};
exports.getallpayments = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM payment',
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};
exports.getpayment = (req, res) => {
    var id = req.params.id;
    connection.query('SELECT * FROM payment WHERE user_influencer_id = ? ', [id],
        function(error, rows) {
            if (error) {
                console.log(error);
            } else {
                res.status(200).json(rows);
            }
        });

};