var express = require('express');
var router = express.Router();


//define every controllers

var ctrlUser = require('../controller/user');
var ctrlCampaign = require('../controller/campaign');
var ctrlJobschedule = require('../controller/jobschedule');
var ctrlPayment = require('../controller/payment');

// User routing

router.post('/addclient', ctrlUser.addclient);
router.post('/addinfluencer', ctrlUser.addinfluencer);
router.post('/login', ctrlUser.login);
router.get('/getadmin/:id', ctrlUser.getadmin);
router.post('/updateadmin', ctrlUser.updateadmin);
router.post('/updateuserpass', ctrlUser.updateuserpass);
router.get('/getclient/:id', ctrlUser.getclient);
router.post('/updateclient', ctrlUser.updateclient);
router.get('/getinfluencer/:id', ctrlUser.getinfluencer);
router.post('/updateinfluencer', ctrlUser.updateinfluencer);
router.get('/getallinfluencers', ctrlUser.getallinfluencers);

//Campaign routing
router.post('/addcampaign', ctrlCampaign.addcampaign);
router.get('/getclientcampaign/:id', ctrlCampaign.getclientcampaign);
router.get('/getcampaignbyid/:id', ctrlCampaign.getcampaignbyid);
router.get('/getallcampaigns', ctrlCampaign.getallcampaigns);
router.get('/adminapprove/:id', ctrlCampaign.adminapprove);
router.get('/adminreject/:id', ctrlCampaign.adminreject);
router.get('/getinfluencercampaign/:id', ctrlCampaign.getinfluencercampaign);
router.post('/updatecampaignstatus', ctrlCampaign.updatecampaignstatus);

//JobSchedule routing
router.post('/addjobschedule', ctrlJobschedule.addjobschedule);
router.get('/getalljobschedules', ctrlJobschedule.getalljobschedules);
router.get('/getalluserjobschedules', ctrlJobschedule.getalluserjobschedules);
router.get('/getjobschedule/:id', ctrlJobschedule.getjobschedule);
router.get('/getinfluencerjobschedules/:id', ctrlJobschedule.getinfluencerjobschedule);
router.get('/getinfluenceruserjobschedules/:id', ctrlJobschedule.getinfluenceruserjobschedule);
router.post('/influencerapprove', ctrlJobschedule.influencerapprove);
router.post('/influencerreject', ctrlJobschedule.influencerreject);

//payment
router.post('/addinvoice', ctrlPayment.addinvoice);
router.get('/getinvoice/:id', ctrlPayment.getinvoice);
router.post('/addpayment', ctrlPayment.addpayment);
router.get('/getjobscheduleid/:id', ctrlPayment.getjobscheduleid);
router.get('/getinfluencerid/:id', ctrlPayment.getinfluencerid);
router.get('/getsocialmediaid/:id', ctrlPayment.getsocialmediaid);
router.get('/getamount/:id', ctrlPayment.getamount);
router.post('/addpayment', ctrlPayment.addpayment);
router.get('/getallpayments', ctrlPayment.getallpayments);
router.get('/getpayment/:id', ctrlPayment.getpayment);
module.exports = router;