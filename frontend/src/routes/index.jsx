import React from 'react';
import {Route} from 'react-router';
import MainPage from "../components/main"
import Superadmin from '../components/superadmin/superadmin';
import Admin from "../components/admin/admin";
import Client from "../components/client/client";
import Influencer from "../components/influencer/influencer";
import {Router, Redirect} from "react-router-dom";
import history from '../history';

function isLoggedIn() {
    if (localStorage.getItem('vinity')) {
        return true;
    }

    return false;
}



export default (
    <Router history={history}>  
        <Route exact path="/"  component={MainPage}/>
        <Route path="/main"  component={MainPage}/>
        <Route path='/superadmin'  render={() => (
            !isLoggedIn() ? (<Redirect to="/main/login" push/>) : (<Superadmin/>))}/>
        <Route path='/admin'  render={() => (
            !isLoggedIn() ? (<Redirect to="/main/login" push/>) : (<Admin/>))}/>
        <Route path='/client'  render={() => (
            !isLoggedIn() ? (<Redirect to="/main/login" push/>) : (<Client/>))}/>
        <Route path='/influencer'  render={() => (
            !isLoggedIn() ? (<Redirect to="/main/login" push/>) : (<Influencer/>))}/>
    </Router>
);
