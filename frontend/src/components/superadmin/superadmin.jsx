import React, {Component} from 'react';
import {Switch} from "react-router";
import {Route} from 'react-router';
import logo from '../../assets/images/logo.png';
import { Navbar, Nav,NavDropdown} from 'react-bootstrap';
import Editsuperadmin from "./editsuperadmin";
import Changepass from "../changepass";
class Superadmin extends Component {
    logout(event) {
        window.localStorage.removeItem('vinity');
        window.location.reload();
    }  
  
    render() {
        return (
            <div>
                 <header>
                     <div className="dashboard_navbar"> 
                         <div className="container d-flex">
                            <a href="/" className="logo">
                                   <img  className="logo-dark default" src={logo} alt=""/>
                            </a>
                            <Navbar collapseOnSelect expand="sm" variant="white" className="ml-auto">
                              <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                              <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="mr-auto">
                                        <NavDropdown title="Edit Profile" >
                                             <NavDropdown.Item href="/superadmin/editprofile">Edit Profile</NavDropdown.Item>
                                             <NavDropdown.Item href="/superadmin/changepass">Change Password</NavDropdown.Item>
                                        </NavDropdown>
                                        <Nav.Link onClick={(e) => {this.logout(e)}}>Logout</Nav.Link>
                                   </Nav>
                                   
                              </Navbar.Collapse>
                         </Navbar>
                         </div>
                     </div>
             </header> 

                <section>
                    <Switch>
                        <Route path="/superadmin/editprofile" component={Editsuperadmin} />
                        <Route path="/superadmin/changepass" component={Changepass} />
                        
                    </Switch>
                </section>
            </div>
        );
    }
}

export default Superadmin;
