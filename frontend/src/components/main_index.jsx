import React, {Component} from 'react';

class MainIndexPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        }
    }
    
    render() {

        return (
               <div className="phone_box" >
                    <p className="phone_title text-center">connecting brand owners <br/> and influencers on social media</p>
                    <p className="phone_body_title">Vinity</p>
                    <div className="form-group text-center">
                         <a className="btn custom_btn" href="./main/clientregister">Brand</a>
                         <a className="btn custom_btn" href="./main/influencerregister">Influencer</a>
                    </div>
               </div>
         );
     }


}

export default MainIndexPage;
