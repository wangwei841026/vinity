import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import history from '../history';
class InfluencerRegister extends Component {
    influencerregister = (event) => {
        event.preventDefault();
        let firstname = event.target.firstname.value;
        let lastname = event.target.lastname.value;
        let email = event.target.email.value;
        let password = event.target.password.value;
        var requestinfluencer={
            firstname:firstname,
            lastname:lastname,
            email:email,
            password:password
       }
       axios.post('http://localhost:4000/api/addinfluencer', requestinfluencer)
               .then(res => {
                    if (res.data==="success") {
                        history.push("/main/login");
                     }
               })
        
        
    }
    render() {
        return (
            <div className="row" style={{background:'#fff',height:'calc(100vh - 72px)'}}>
                    <div className="col-12 col-md-4 mx-auto pl-0">
                        
                        <form onSubmit={this.influencerregister}>
                            <h1 className="h3 my-5 font-weight-normal">Influencer Register</h1>
                            {/* {!isSuccess ? <div className="errortext">{message}</div> : <Redirect push to="/dashboard"/>} */}
                            <div className="form-group">
                                <input type="text"  name="firstname" className="form-control " placeholder="First name" required />
                            </div>
                            <div className="form-group">
                                <input type="text"  name="lastname" className="form-control " placeholder="Last name" required />
                            </div>
                            <div className="form-group">
                                <input type="email"  name="email" className="form-control " placeholder="Email address" required />
                            </div>
                            <div className="form-group">
                                <input type="password"  name="password" className="form-control" placeholder="Password" required />
                            </div>
                            
                            <button className="form-control login-btn mt-4" type="submit" >Register</button>
                           
                            <div className="d-flex align-items-center mt-5">
                                <span className="registertxt ml-3"><Link to='login'>Login</Link></span> 
                            </div>
                        </form>
                       
                    </div>
            </div>
        )
    }
}

export default InfluencerRegister;
