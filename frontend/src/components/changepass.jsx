import React, {Component} from 'react';
import axios from 'axios';

class Changepass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success:""
        }
    }
    
    updatepass= (e) => {
        var self=this;
        e.preventDefault();
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        var requestpass={
             id:ruser[0].id,
             password:this.password.value,
        }
        axios.post('http://localhost:4000/api/updateuserpass',requestpass)
            .then(res => {
                console.log(res);
                 if (res.data==="Success") {
                     self.setState({success:"success"});
                    
                  }
            })
    }
    
    render() {
       
        return (
            <div className="row">
                <div className="pl-0 mx-auto col-10  col-sm-6 col-md-4 col-lg-3">
                    <form onSubmit={this.updatepass}>
                        <h1 className="h3 my-5 font-weight-normal">Change Password</h1>
                        <div className="form-group ">
                            <label>Password</label>
                            <input type="password"  name="password" className="form-control  inputtxt" placeholder="Password" required />
                        </div>
                        <div className="form-group ">
                            <label>New Password</label>
                            <input type="password"  name="password" className="form-control  inputtxt" placeholder="New Password" 
                             ref={el => this.password = el} required />
                        </div>
                        <button className="form-control inputtxt login-btn mt-5 p-0" type="submit" >Change</button>
                       
                            {this.state.success? <div className="alert alert-primary mt-5" role="alert">Success</div> :""}
                    </form>
                    
                </div>
            </div>
        );
    }


}


export default Changepass;
