import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';
import { Link } from 'react-router-dom';
class AdminCampaign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            updatemodal: false,
            tablelist:[],
            campaigns:[],
            updatecampaign:[],
            success:"",
            invoicemodal:false,
            amount:"",
        
        };
        this.openupdateModal = this.openupdateModal.bind(this);
        this.closeupdateModal = this.closeupdateModal.bind(this);
        this.openinvoiceModal = this.openinvoiceModal.bind(this);
        this.closeinvoiceModal = this.closeinvoiceModal.bind(this);
    

    }
    openupdateModal() {
        this.setState({updatemodal: true});
    }
     
    closeupdateModal() {
        this.setState({updatemodal: false});
    }
    openinvoiceModal() {
        this.setState({invoiceemodal: true});
    }
     
    closeinvoiceModal() {
        this.setState({invoicemodal: false});
    }
    
    componentWillMount(){
        var self=this;
        var result = [];
        var actiontag;
        axios.get('http://localhost:4000/api/getallcampaigns/')
             .then(response => {
                  console.log(response.data);
                  this.setState({campaigns: response.data});
                       response.data.map(function(campaign, i){
                           if (campaign.status===1) {
                               actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatefirststatus(e, campaign.id)}}>Approve/Reject</MDBBtn>
                           } else if (campaign.status===2)  {
                               if (campaign.substatus!==null) {
                                    actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatesecondstatus(e, campaign.id)}}>Change Status</MDBBtn>
                               } else {
                                    actiontag=<Link to={`/admin/createjobshedule/${campaign.id}`}><MDBBtn color="orange" className="removebtn" outline size="sm">Create Jobschedule</MDBBtn></Link>
                               }
                            } else if (campaign.status===4) {
                                if (campaign.substatus==="invoicecreated") {
                                    actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatethirdstatus(e, campaign.id)}}>Change Status</MDBBtn>
                                    
                                } else {
                                    actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.openinvoice(e, campaign.id)}}>Create Invoice</MDBBtn>
                                }
                                
                            } else if (campaign.status===5) {
                                actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updateforthstatus(e, campaign.id)}}>Change Status</MDBBtn>
                            } else if (campaign.status===6) {
                                actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatefifthstatus(e, campaign.id)}}>Change Status</MDBBtn>
                            } else if (campaign.status===7) {
                                actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatesixthstatus(e, campaign.id)}}>Change Status</MDBBtn>
                                
                            
                            }  else {
                                actiontag="";
                            }
                            return result.push({"no":(i+1), "client":campaign.first_name,
                            "productname":campaign.product_name, 
                            "productcategory":campaign.product_category_id, 
                            "productdescription":campaign.product_description,
                            "promotiongoal":campaign.promotion_goal_id,
                            "promotionbudget":campaign.promotion_budget,
                            "promotiondescription":campaign.promotion_description,
                            "status":self.getstatusname(campaign.status),
                            "createddate":self.timeConverter(campaign.created_date),
                            "modifieddate":self.timeConverter(campaign.modified_date),
                            "action":actiontag,
                         });
                                                
                       });
                  this.setState({tablelist: result});
            }) 
           
    }
    
    updatefirststatus(e,id) {
        this.setState({updatemodal: true});
        
        var updatecampaign=this.state.campaigns.find((e) => e.id === id);
        this.setState({updatecampaign:updatecampaign});
        console.log(updatecampaign);
        
    }
    updatesecondstatus(e,id) {
        var req={
            status:3,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    openinvoice(e,id) {
        this.setState({invoicemodal: true});
        
        var updatecampaign=this.state.campaigns.find((e) => e.id === id);
        this.setState({updatecampaign:updatecampaign});
        console.log(updatecampaign);
        
    }
    openpayment(e,id) {
        this.setState({paymentmodal: true});
        var updatecampaign=this.state.campaigns.find((e) => e.id === id);
        this.setState({updatecampaign:updatecampaign});
        console.log(updatecampaign);
        
    }
    updatethirdstatus(e,id) {
        var req={
            status:5,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    updateforthstatus(e,id) {
        var req={
            status:6,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    updatefifthstatus(e,id) {
        var req={
            status:7,
            id:id
        }
      
        var jobscheduleid;
        var influencerid;
        var socialmediaid;
        var price;
        axios.get('http://localhost:4000/api/getjobscheduleid/'+req.id)
        .then(response => {
           
           jobscheduleid=response.data[0]["id"];
           axios.get('http://localhost:4000/api/getinfluencerid/'+jobscheduleid)
            .then(response => {
               
                influencerid=response.data[0]["user_influencer_id"];
                axios.get('http://localhost:4000/api/getsocialmediaid/'+jobscheduleid)
                    .then(response => {
                    
                    socialmediaid=response.data[0]["socialmedia_id"];
                    axios.get('http://localhost:4000/api/getamount/'+socialmediaid)
                        .then(response => {
                       
                        price=response.data[0]["price"];
                        var reqpayment={
                            campaignid:req.id,
                            influencerid:influencerid,
                            jobscheduleid:jobscheduleid,
                            amount:price
                        }
                        axios.post('http://localhost:4000/api/addpayment/',reqpayment)
                            .then(response => {
                                if (response.data==="success") {
                                    window.location.reload();
                                }
                        
                        });

                    });
                        

                });

            });

        });
        
      
    }
    updatesixthstatus(e,id) {
        var req={
            status:8,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    approve(e,id) {
        var self=this;
        e.preventDefault();
        axios.get('http://localhost:4000/api/adminapprove/'+id)
            .then(res => {
                console.log(res.data);
                 if (res.data==="success") {
                      self.closeupdateModal();
                      window.location.reload();
                  }
            })
    }
    reject(e,id) {
        var self=this;
        e.preventDefault();
        axios.get('http://localhost:4000/api/adminreject/'+id)
            .then(res => {
                console.log(res.data);
                 if (res.data==="success") {
                      self.closeupdateModal();
                      window.location.reload();
                  }
            })
    }
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getstatusname(id){
        if (id===1) {
            return "Waiting VInity's approval";
        } else if (id===2) {
            return "Waiting VInity's recommendation";
        } else if (id===3) {
            return "Waiting Client's feedback";
        }  else if (id===4) {
            return "Waiting Influencer's feedback";
        } else if (id===5) {
            return "Waiting for payments";
        } else if (id===6) {
            return "Payment accepted";
        } else if (id===7) {
            return "Marketing Execution";
        } else if (id===8) {
            return "Done";
        } else if (id===9) {
            return "Rejected";
        }
    
    }
    onChange = (e) => {
          
        this.setState({ [e.target.name]: e.target.value });

    }  
    createinvoice=(e) => {
        var self=this;
        e.preventDefault();
        var requestinvoice={
             clientid:this.state.updatecampaign['user_client_id'],
             campaignid:this.state.updatecampaign['id'],
             amount:this.state.amount
        }
        axios.post('http://localhost:4000/api/addinvoice', requestinvoice)
            .then(res => {
                console.log(res.data);
                 if (res.data==="success") {
                     self.closeinvoiceModal();
                     window.location.reload();
                 }
            })
   }   
   createpayment=(e) => {
    var self=this;
    e.preventDefault();
    var requestinvoice={
         clientid:this.state.updatecampaign['user_client_id'],
         campaignid:this.state.updatecampaign['id'],
         amount:this.state.amount
    }
    axios.post('http://localhost:4000/api/addinvoice', requestinvoice)
        .then(res => {
            console.log(res.data);
             if (res.data==="success") {
                 self.closeinvoiceModal();
                 window.location.reload();
             }
        })
}   
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Client',
                field: 'client',
                width: 250
              },
              {
                label: 'Product Name',
                field: 'productname',
                width: 250
              },
              {
                 label: 'Product Category',
                 field: 'productcategory',
                 width: 250
               },
              {
                label: 'Product Description',
                field: 'productdescription',
                width: 250
              },
              {
                 label: 'Promotion Goal',
                 field: 'promotiongoal',
                 width: 250
               },
               {
                 label: 'Promotion Budget',
                 field: 'promotionbudget',
                 width: 250
               },
               {
                 label: 'Promotion Description',
                 field: 'promotiondescription',
                 width: 200
               },
               {
                label: 'Status',
                field: 'status',
                width: 300
              },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              {
                label: 'Action',
                field: 'action',
                width: 200
              },
             
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">Campaigns</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         
                </div>
                <div>
                    <Modal show={this.state.updatemodal} onHide={this.closeupdateModal} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Client Campaign</Modal.Title>
                        </Modal.Header>
                        
                            <Modal.Body>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Name</label>
                                    <input type="text"  name="productname" className="form-control col-9 inputtxt" placeholder="Product Name" defaultValue={this.state.updatecampaign['product_name']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Categoryid</label>
                                    <input type="text"  name="productcategory" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['product_category_id']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Description</label>
                                    <input type="text"  name="productdescription" className="form-control col-9 inputtxt" placeholder="Product Description" defaultValue={this.state.updatecampaign['product_description']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Goalid</label>
                                    <input type="text"  name="promotiongoal" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['promotion_goal_id']}  required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Budget</label>
                                    <input type="text"  name="promotionbudget" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['promotion_budget']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Description</label>
                                    <input type="text"  name="promotiondescription" className="form-control col-9 inputtxt" placeholder="Promotion Description" defaultValue={this.state.updatecampaign['promotion_description']}  required />
                                </div>
                                    
                            </Modal.Body> 
                        <Modal.Footer>
                            <Button variant="primary"  onClick={(e) => {this.approve(e, this.state.updatecampaign['id'])}}>Approve</Button>
                            <Button variant="primary"  onClick={(e) => {this.reject(e, this.state.updatecampaign['id'])}}>Reject</Button>
                            <Button variant="light"  onClick={this.closeupdateModal}>Close</Button>
                            
                        </Modal.Footer>  
                                       
                    </Modal>                    
                    </div>
                    <div>
                    <Modal show={this.state.invoicemodal} onHide={this.closeinvoiceModal} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Create Invoice</Modal.Title>
                        </Modal.Header>
                        <form onSubmit={this.createinvoice} className="p-3">
                            <Modal.Body>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Amount</label>
                                    <input type="text"  name="amount" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange} required />
                                </div>
                            </Modal.Body> 
                        <Modal.Footer>
                            <Button variant="primary"  type="submit">Create</Button>
                            <Button variant="light"  onClick={this.closeinvoiceModal}>Close</Button>
                            
                        </Modal.Footer>  
                        </form>               
                    </Modal>                    
                    </div>
                   
                   
            </div>
            
        );
    }


}


export default AdminCampaign;
