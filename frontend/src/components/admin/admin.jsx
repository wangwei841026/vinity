import React, {Component} from 'react';
import {Switch} from "react-router";
import {Route} from 'react-router';
import logo from '../../assets/images/logo.png';
import { Navbar, Nav,NavDropdown} from 'react-bootstrap';
import Editadmin from "./editadmin";
import Changepass from "../changepass";
import AdminCampaign from "./admincampaign";
import AdminJobSchedule from "./adminjobschedule";
import AdminUserJobSchedule from "./adminuserjobschedule";
import AdminCreateJobSchedule from "./admincreatejobschedule";
import AdminPayment from './adminpayment';

class Admin extends Component {
    logout(event) {
        window.localStorage.removeItem('vinity');
        window.location.reload();
    }  
  
    render() {
        return (
            <div>
                 <header>
                     <div className="dashboard_navbar"> 
                         <div className="container d-flex">
                            <a href="/" className="logo">
                                   <img  className="logo-dark default" src={logo} alt=""/>
                            </a>
                            <Navbar collapseOnSelect expand="sm" variant="white" className="ml-auto">
                              <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                              <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="mr-auto">
                                        <Nav.Link href="/admin/campaign">Campaign</Nav.Link>
                                        <Nav.Link href="/admin/payment">Payment</Nav.Link>
                                        <NavDropdown title="Schedule" >
                                             <NavDropdown.Item href="/admin/jobschedule">Job Schedule</NavDropdown.Item>
                                             <NavDropdown.Item href="/admin/userjobschedule">User Job Schedule</NavDropdown.Item>
                                        </NavDropdown>
                                        <NavDropdown title="Edit Profile" >
                                             <NavDropdown.Item href="/admin/editprofile">Edit Profile</NavDropdown.Item>
                                             <NavDropdown.Item href="/admin/changepass">Change Password</NavDropdown.Item>
                                        </NavDropdown>
                                        <Nav.Link onClick={(e) => {this.logout(e)}}>Logout</Nav.Link>
                                   </Nav>
                                   
                              </Navbar.Collapse>
                         </Navbar>
                         </div>
                     </div>
             </header> 

                <section>
                    <Switch>
                        <Route path="/admin/editprofile" component={Editadmin} />
                        <Route path="/admin/changepass" component={Changepass} />
                        <Route path="/admin/campaign" component={AdminCampaign} />
                        <Route path="/admin/jobschedule" component={AdminJobSchedule} />
                        <Route path="/admin/userjobschedule" component={AdminUserJobSchedule} />
                        <Route path="/admin/createjobshedule/:campaignid" component={AdminCreateJobSchedule} />
                        <Route path="/admin/payment" component={AdminPayment} />
                    </Switch>
                </section>
            </div>
        );
    }
}

export default Admin;
