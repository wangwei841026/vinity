import React, {Component} from 'react';
import axios from 'axios';

import { MDBDataTable} from 'mdbreact';
class AdminUserJobSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userjobschedule:[],
            tablelist:[]
        };
       
    }
   
    componentWillMount(){
        var self=this;
        var result = [];
      
        axios.get('http://localhost:4000/api/getalluserjobschedules/')
             .then(response => {
                  console.log(response.data);
                  this.setState({userjobschedule: response.data});
                       response.data.map(function(userjobschedule, i){
                            return result.push({"no":(i+1), "jobscheduleid":userjobschedule.job_schedule_id, 
                            "influencer":userjobschedule.user_influencer_id, 
                            "reportimage":userjobschedule.report_image_url,
                            "reportlink":userjobschedule.report_link,
                            "status":self.getjobstatusname(userjobschedule.status),
                            "createddate":self.timeConverter(userjobschedule.created_date),
                            "modifieddate":self.timeConverter(userjobschedule.modified_date),
                           
                         });
                                                
                       });
                  this.setState({tablelist: result});
             
        })   
    }
    
   
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getjobstatusname(id){
        if (id===0) {
            return "Pending";
        } else if (id===1) {
            return "Waiting Influencers Approval";
        } else if (id===2) {
            return "Approved";
        }  else if (id===3) {
            return "Running";
        } else if (id===4) {
            return "Done";
        } else if (id===5) {
          return "Rejected";
      }
    
    }
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Job Schedule Id',
                field: 'jobscheduleid',
                width: 250
              },
              {
                label: 'InfluencerId',
                field: 'influencer',
                width: 250
              },
              {
                 label: 'Report Image',
                 field: 'reportimage',
                 width: 250
               },
              {
                label: 'Report Link',
                field: 'reportlink',
                width: 250
              },
               {
                 label: 'Status',
                 field: 'status',
                 width: 200
               },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">User JobSchedules</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         
                </div>
            </div>
            
        );
    }


}


export default AdminUserJobSchedule;
