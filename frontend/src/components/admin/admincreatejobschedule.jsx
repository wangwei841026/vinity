import React, {Component} from 'react';
import axios from 'axios';
import ComboSelect from 'react-combo-select';
import {Redirect} from "react-router-dom";
require('../../../node_modules/react-combo-select/style.css');
var reqinfluencers;
class AdminCreateJobSchedule extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            influencers:[],
            clientid:"",
            campaignid:"",
            jobdate:"",
            caption:"",
            mediatype:"",
            mediaurl:"",
            categorytarget:"",
            reqinfluencers:[],
            reportimage:"",
            reportlink:"",
            success:""

        };
        this.setinfluencers = this.setinfluencers.bind(this); 
    }
 
    componentWillMount(){
      const { match: { params } } = this.props;
      this.setState({campaignid:params.campaignid});
      var result=[];
     
      axios.get('http://localhost:4000/api/getcampaignbyid/'+params.campaignid)
        .then(response => {
            
            this.setState({clientid:response.data[0].user_client_id});
            axios.get('http://localhost:4000/api/getallinfluencers/')
            .then(response=>{
               
                
                response.data.forEach(function(ele, i){ 
                    result.push({text:ele.first_name, value:ele.user_id.toString()});
                })
                this.setState({influencers:result});
            })
      });
        
      
        
    }
    addjobschedule=(e) => {
        
        e.preventDefault();
        
        var requestjob={
             clientid:this.state.clientid,
             campaignid:this.state.campaignid,
             jobdate:this.state.jobdate,
             caption:this.state.caption,
             mediatype:this.state.mediatype,
             mediaurl:this.state.mediaurl,
             categorytarget:this.state.categorytarget,
             reqinfluencers:reqinfluencers,
             reportimage:this.state.reportimage,
             reportlink:this.state.reportlink
        }
       
        axios.post('http://localhost:4000/api/addjobschedule', requestjob)
            .then(res => {
                if (res.data==="success") {
                    console.log(res.data);
                    this.setState({success:"success"});
                }
                 
            })
   }
    onChange = (e) => {
            
        this.setState({ [e.target.name]: e.target.value });

    }     
    setinfluencers=(e)=> {        
        
        reqinfluencers=e;
    }
   
    render() {
        return (
            <div className="row">
                <div className="col-md-10 col-lg-8 p-5 mx-auto">
                    {this.state.success==="success" ?  <Redirect push to="/admin/campaign"/> : ""}
                    <h3>Job Schdule</h3>
                    <form onSubmit={this.addjobschedule} className="p-3">
                        <div className="form-group row ">
                            <label className="col-3 align-self-center mb-0">Job Date</label>
                            <input type="text"  name="jobdate" className="form-control col-9 inputtxt" placeholder="Job Date" onChange={this.onChange} required />
                        </div>
                        <div className="form-group row ">
                            <label className="col-3 align-self-center mb-0">Caption</label>
                            <input type="text"  name="caption" className="form-control col-9 inputtxt" placeholder="Caption" onChange={this.onChange} required />
                        </div>
                        <div className="form-group row ">
                            <label className="col-3 align-self-center mb-0">Media Type</label>
                            <input type="text"  name="mediatype" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange} required />
                        </div>
                        <div className="form-group row ">
                            <label className="col-3 align-self-center mb-0">Media Url</label>
                            <input type="text"  name="mediaurl" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange} required />
                        </div>
                        <div className="form-group row ">
                            <label className="col-3 align-self-center mb-0">Category Target</label>
                            <input type="text"  name="categorytarget" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange}  required />
                        </div> 
                        <h3 className="mt-5">User Job Schedule</h3>
                            
                            <div className="form-group row ">
                                <label className="col-3 align-self-center mb-0">Report Image</label>
                                <input type="text"  name="reportimage" className="form-control col-9 inputtxt" placeholder="Report Image Url"  onChange={this.onChange} />
                            </div>
                            <div className="form-group row ">
                                <label className="col-3 align-self-center mb-0">Report Link</label>
                                <input type="text"  name="reportlink" className="form-control col-9 inputtxt" placeholder="Report Link" onChange={this.onChange} />
                            </div>
                            <div className="form-group row ">
                                <label className="col-3 align-self-center mb-0">Influencer</label>
                                <ComboSelect data={this.state.influencers} type="multiselect" onChange={this.setinfluencers}  />
                            </div>    
                        <button className="form-control col-3 inputtxt login-btn mt-5 p-0" type="submit" >Create</button>
                    </form>
                </div>
            </div>
            
        );
    }


}


export default AdminCreateJobSchedule;
