import React, {Component} from 'react';
import axios from 'axios';
import { MDBDataTable} from 'mdbreact';
class AdminJobSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobschedule:[],
            tablelist:[]
        };
       
    }
   
    componentWillMount(){
        var self=this;
        var result = [];
      
        axios.get('http://localhost:4000/api/getalljobschedules/')
             .then(response => {
                  console.log(response.data);
                  this.setState({jobschedule: response.data});
                       response.data.map(function(jobschedule, i){
                            return result.push({"no":(i+1), "campaignid":jobschedule.campaign_id, 
                            "jobdate":jobschedule.job_date, 
                            "caption":jobschedule.caption,
                            "mediatype":jobschedule.media_type,
                            "mediaurl":jobschedule.media_url,
                            "categorytarget":jobschedule.category_target,
                            "status":self.getjobstatusname(jobschedule.status),
                            "createddate":self.timeConverter(jobschedule.created_date),
                            "modifieddate":self.timeConverter(jobschedule.modified_date),
                           
                         });
                                                
                       });
                  this.setState({tablelist: result});
             
        })   
    }
    
   
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getjobstatusname(id){
        if (id===0) {
            return "Pending";
        } else if (id===1) {
            return "Waiting Influencers Approval";
        } else if (id===2) {
            return "Approved";
        }  else if (id===3) {
            return "Running";
        } else if (id===4) {
            return "Done";
        } else if (id===5) {
          return "Rejected";
      }
    
    }
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Campaign Id',
                field: 'campaignid',
                width: 250
              },
              {
                label: 'Job Date',
                field: 'jobdate',
                width: 250
              },
              {
                 label: 'Caption',
                 field: 'caption',
                 width: 250
               },
              {
                label: 'Media Type',
                field: 'mediatype',
                width: 250
              },
              {
                 label: 'Media Url',
                 field: 'mediaurl',
                 width: 250
               },
               {
                 label: 'Category Target',
                 field: 'categorytarget',
                 width: 250
               },
               {
                 label: 'Status',
                 field: 'status',
                 width: 200
               },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">JobSchedules</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         
                </div>
            </div>
            
        );
    }


}


export default AdminJobSchedule;
