import React, {Component} from 'react';
import axios from 'axios';

class Editadmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initialuser:[],
            success:''
        }
    }
    componentWillMount(){
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getadmin/'+ruser[0].id)
             .then(response => {
                this.setState({initialuser: response.data});
                
             })
             .catch(function (error) {
                  console.log(error);
             })
    }
    updateadmin= (e) => {
        var self=this;
        e.preventDefault();
        var requestuser={
             id:this.state.initialuser[0]["id"],
             email:this.email.value,
        }
        axios.post('http://localhost:4000/api/updateadmin',requestuser)
            .then(res => {
                console.log(res);
                 if (res.data==="Success") {
                     self.setState({success:"success"});
                    
                  }
            })
    }
    
    render() {
       
        return (
            <div className="row">
                <div className="pl-0 mx-auto col-10  col-sm-6 col-md-4 col-lg-3">
                    <form onSubmit={this.updateadmin}>
                        <h1 className="h3 my-5 font-weight-normal">Edit Profile</h1>
                        <div className="form-group ">
                            <label>Email</label>
                            <input type="email"  name="email" className="form-control  inputtxt" placeholder="Email address" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['email']:""}  ref={el => this.email = el} required />
                        </div>
                        <button className="form-control inputtxt login-btn mt-5 p-0" type="submit" >Edit</button>
                       
                            {this.state.success? <div className="alert alert-primary mt-5" role="alert">Success</div> :""}
                    </form>
                    
                </div>
            </div>
        );
    }


}


export default Editadmin;
