import React, {Component} from 'react';
import history from '../history';
import axios from 'axios';
class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message:""
        }
    }
    onHandleLogin = (event) => {
        event.preventDefault();
        let email = event.target.email.value;
        let password = event.target.password.value;

        var loginuser={
            email:email,
            password:password
       }
        axios.post('http://localhost:4000/api/login', loginuser)
        .then(res => {
            console.log(res.data);
             if (res.data['success']===true) {
                 console.log(res.data['user'][0]['user_type_id']);
                 localStorage.setItem("vinity", JSON.stringify(res.data['user']));
                 if (res.data['user'][0]['user_type_id']===10) {
                    history.push("/superadmin");
                 } else if  (res.data['user'][0]['user_type_id']===0) {
                    history.push("/admin");
                 } else if  (res.data['user'][0]['user_type_id']===1) {
                    history.push("/client");
                 } else if  (res.data['user'][0]['user_type_id']===2) {
                    history.push("/influencer");
                 }
               
              } else {
                this.setState({message: res.data['message']});
              }
        })
    }
    
    render() {
        return (
            <div className="row" style={{background:'#fff',height:'calc(100vh - 72px)'}}>
                    <div className="col-12 col-md-4 mx-auto pl-0">
                        
                        <form onSubmit={this.onHandleLogin}>
                            <h1 className="h3 my-5 font-weight-normal">Login</h1>
                            {this.state.message!=="" ? <div className="errortext">{this.state.message}</div> : ""}
                           
                            <div className="form-group">
                                <input type="email"  name="email" className="form-control " placeholder="Email address" required />
                            </div>
                            <div className="form-group">
                                <input type="password"  name="password" className="form-control" placeholder="Password" required />
                            </div>
                            
                            <button className="form-control login-btn mt-4" type="submit" >Login</button>
                            <div className="mt-5 text-center">
                                <span className="registertxt"> Don't have account?</span>
                                <a href="./clientregister" className="form-control login-btn mt-3" >Register Client</a>
                                <a href="./influencerregister" className="form-control influencer-btn mt-3" >Register Influencer</a>
                            </div>
                        </form>
                        <div className="mt-5 text-center">
                            <button ><i className="fab fa-facebook-square"></i></button>
                            <button className="ml-3"><i className="fab fa-google"></i></button>
                            <button className="ml-3"><i className="fab fa-twitter"></i></button>
                            <button className="ml-3"><i className="fab fa-instagram"></i></button>
                        </div>
                    </div>
            </div>
        );
    }


}


export default LoginPage;
