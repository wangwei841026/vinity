import React, {Component} from 'react';
import axios from 'axios';
import { MDBDataTable} from 'mdbreact';
class InfluencerCampaign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            campaigns:[],
            tablelist:[]
        };
       
       
    }
  
    componentWillMount(){
        var self=this;
        var result = [];
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getinfluencercampaign/'+ruser[0].id)
             .then(response => {
                  console.log(response.data);
                  this.setState({campaigns: response.data});
                       response.data.map(function(campaign, i){
                            return result.push({"no":(i+1), "productname":campaign.product_name, 
                            "productcategory":campaign.product_category_id, 
                            "productdescription":campaign.product_description,
                            "promotiongoal":campaign.promotion_goal_id,
                            "promotionbudget":campaign.promotion_budget,
                            "promotiondescription":campaign.promotion_description,
                            "status":self.getstatusname(campaign.status),
                            "createddate":self.timeConverter(campaign.created_date),
                            "modifieddate":self.timeConverter(campaign.modified_date),
                       });
                    });
                this.setState({tablelist: result});
            }) 
            
    }
   
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getstatusname(id){
        if (id===1) {
            return "Waiting VInity's approval";
        } else if (id===2) {
            return "Waiting VInity's recommendation";
        } else if (id===3) {
            return "Waiting Client's feedback";
        }  else if (id===4) {
            return "Waiting Influencer's feedback";
        } else if (id===5) {
            return "Waiting for payments";
        } else if (id===6) {
            return "Payment accepted";
        } else if (id===7) {
            return "Marketing Execution";
        } else if (id===8) {
            return "Done";
        } else if (id===9) {
            return "Done";
        } else if (id===10) {
            return "Client Rejected";
        }
    
    }
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Product Name',
                field: 'productname',
                width: 250
              },
              {
                 label: 'Product Category',
                 field: 'productcategory',
                 width: 250
               },
              {
                label: 'Product Description',
                field: 'productdescription',
                width: 250
              },
              {
                 label: 'Promotion Goal',
                 field: 'promotiongoal',
                 width: 250
               },
               {
                 label: 'Promotion Budget',
                 field: 'promotionbudget',
                 width: 250
               },
               {
                 label: 'Promotion Description',
                 field: 'promotiondescription',
                 width: 200
               },
               {
                label: 'Status',
                field: 'status',
                width: 200
              },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              }
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">Campaigns</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                </div>
            </div>
            
        );
    }


}


export default InfluencerCampaign;
