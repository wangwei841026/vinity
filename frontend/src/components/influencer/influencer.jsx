import React, {Component} from 'react';
import {Switch} from "react-router";
import {Route} from 'react-router';
import logo from '../../assets/images/logo.png';
import { Navbar, Nav,NavDropdown} from 'react-bootstrap';
import Editinfluencer from "./editinfluencer";
import Changepass from "../changepass";
import InfluencerCampaign from "./influencercampaign";
import InfluencerJobSchedule from "./influencerjobschedule";
import InfluencerUserJobSchedule from "./influenceruserjobschedule";
import InfluencerPayment from "./influencerpayment";
class Influencer extends Component {
    logout(event) {
        window.localStorage.removeItem('vinity');
        window.location.reload();
    }  
  
    render() {
        return (
            <div>
                 <header>
                     <div className="dashboard_navbar"> 
                         <div className="container d-flex">
                            <a href="/" className="logo">
                                   <img  className="logo-dark default" src={logo} alt=""/>
                            </a>
                            <Navbar collapseOnSelect expand="sm" variant="white" className="ml-auto">
                              <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                              <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="mr-auto">
                                        <Nav.Link href="/influencer/campaign">Campaign</Nav.Link>
                                        <Nav.Link href="/influencer/payment">Payment</Nav.Link>
                                        <NavDropdown title="Schedule" >
                                             <NavDropdown.Item href="/influencer/jobschedule">Job Schedule</NavDropdown.Item>
                                             <NavDropdown.Item href="/influencer/userjobschedule">User Job Schedule</NavDropdown.Item>
                                        </NavDropdown>
                                        <NavDropdown title="Edit Profile" >
                                             <NavDropdown.Item href="/influencer/editprofile">Edit Profile</NavDropdown.Item>
                                             <NavDropdown.Item href="/influencer/changepass">Change Password</NavDropdown.Item>
                                        </NavDropdown>
                                        <Nav.Link onClick={(e) => {this.logout(e)}}>Logout</Nav.Link>
                                   </Nav>
                                   
                              </Navbar.Collapse>
                         </Navbar>
                         </div>
                     </div>
             </header> 

                <section>
                    <Switch>
                        <Route path="/influencer/editprofile" component={Editinfluencer} />
                        <Route path="/influencer/changepass" component={Changepass} />
                        <Route path="/influencer/campaign" component={InfluencerCampaign} />
                        <Route path="/influencer/jobschedule" component={InfluencerJobSchedule} />
                        <Route path="/influencer/userjobschedule" component={InfluencerUserJobSchedule} />
                        <Route path="/influencer/payment" component={InfluencerPayment} />
                    </Switch>
                </section>
            </div>
        );
    }
}

export default Influencer;
