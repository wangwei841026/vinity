import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';
class InfluencerUserJobSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userjobschedule:[],
            tablelist:[],
            updatemodal: false,
            updateschedule:[]
        };
        this.openupdateModal = this.openupdateModal.bind(this);
        this.closeupdateModal = this.closeupdateModal.bind(this);
       
    }
    openupdateModal() {
      this.setState({updatemodal: true});
    }
   
    closeupdateModal() {
        this.setState({updatemodal: false});
    }
    componentWillMount(){
        var self=this;
        var result = [];
        var actiontag;
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getinfluenceruserjobschedules/'+ruser[0].id)
             .then(response => {
                  console.log(response.data);
                  this.setState({userjobschedule: response.data});
                       response.data.map(function(userjobschedule, i){
                            if (userjobschedule.status===0) {
                                actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatefirststatus(e, userjobschedule.id)}}>Approve/Reject</MDBBtn>
                            } else {
                                actiontag="";
                            }
                            return result.push({"no":(i+1), 
                            "client":userjobschedule.client_id,
                            "jobscheduleid":userjobschedule.job_schedule_id, 
                            "reportimage":userjobschedule.report_image_url,
                            "reportlink":userjobschedule.report_link,
                            "status":self.getjobstatusname(userjobschedule.status),
                            "createddate":self.timeConverter(userjobschedule.created_date),
                            "modifieddate":self.timeConverter(userjobschedule.modified_date),
                            "action":actiontag
                         });
                                                
                       });
                  this.setState({tablelist: result});
             
        })   
    }
    updatefirststatus(e,id) {
      this.setState({updatemodal: true});
      
      var updatejobschedule=this.state.userjobschedule.find((e) => e.id === id);
      this.setState({updateschedule:updatejobschedule});
      
  }
   
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getjobstatusname(id){
        if (id===0) {
            return "Pending";
        } else if (id===1) {
            return "Waiting Influencers Approval";
        } else if (id===2) {
            return "Approved";
        }  else if (id===3) {
            return "Running";
        } else if (id===4) {
            return "Done";
        }  else if (id===5) {
          return "Rejected";
      }
    
    }
    approve(e,id,scheduleid) {
        var self=this;
        e.preventDefault();
        var reqid={
            id:id,
            jobid:scheduleid
        }
        axios.post('http://localhost:4000/api/influencerapprove',reqid)
            .then(res => {
                console.log(res.data);
                if (res.data==="success") {
                        self.closeupdateModal();
                        window.location.reload();
                    }
        })
    }
    reject(e,id,scheduleid) {
        var self=this;
        e.preventDefault();
        var reqid={
        id:id,
        jobid:scheduleid
        }
        axios.post('http://localhost:4000/api/influencerreject',reqid)
            .then(res => {
                console.log(res.data);
                if (res.data==="success") {
                    self.closeupdateModal();
                    window.location.reload();
                }
            })
    }
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Client',
                field: 'client',
                width: 250
              },
              {
                label: 'Job Schedule Id',
                field: 'jobscheduleid',
                width: 250
              },
              {
                 label: 'Report Image',
                 field: 'reportimage',
                 width: 250
               },
              {
                label: 'Report Link',
                field: 'reportlink',
                width: 250
              },
               {
                 label: 'Status',
                 field: 'status',
                 width: 200
               },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              {
                label: 'action',
                field: 'action',
                width: 200
              }
              
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">User JobSchedules</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         
                </div>
                <div>
                    <Modal show={this.state.updatemodal} onHide={this.closeupdateModal} size="lg">
                            <Modal.Body>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Client</label>
                                    <input type="text"  name="productname" className="form-control col-9 inputtxt" placeholder="Product Name" defaultValue={this.state.updateschedule['client_id']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Job Scheduleid</label>
                                    <input type="text"  name="productcategory" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updateschedule['job_schedule_id']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Report Image</label>
                                    <input type="text"  name="productdescription" className="form-control col-9 inputtxt" placeholder="Product Description" defaultValue={this.state.updateschedule['report_image_url']} />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Report Link</label>
                                    <input type="text"  name="promotiongoal" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updateschedule['report_link']}  required />
                                </div>
                                
                                    
                            </Modal.Body> 
                        <Modal.Footer>
                            <Button variant="primary"  onClick={(e) => {this.approve(e, this.state.updateschedule['id'],this.state.updateschedule['job_schedule_id'])}}>Approve</Button>
                            <Button variant="primary"  onClick={(e) => {this.reject(e, this.state.updateschedule['id'], this.state.updateschedule['job_schedule_id'])}}>Reject</Button>
                            <Button variant="light"  onClick={this.closeupdateModal}>Close</Button>
                            
                        </Modal.Footer>  
                                       
                    </Modal>                    
                    </div>
            </div>
            
        );
    }


}


export default InfluencerUserJobSchedule;
