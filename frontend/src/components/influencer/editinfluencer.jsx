import React, {Component} from 'react';
import axios from 'axios';
import DatePicker from "react-datepicker";
class Editinfluencer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initialuser:[],
            success:'',
            birthday:''
        }
        this.birthdayChange = this.birthdayChange.bind(this);
    }
    componentWillMount(){
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getinfluencer/'+ruser[0].id)
             .then(response => {
                 console.log(response.data);
                this.setState({initialuser: response.data});
                
             })
             .catch(function (error) {
                  console.log(error);
             })
    }
    birthdayChange(date) {
        this.setState({birthday: date});
    }
    updateclient= (e) => {
        var self=this;
        e.preventDefault();
        var requestuser={
             id:this.state.initialuser[0]["id"],
             user_id:this.state.initialuser[0]["user_id"],
             email:this.email.value,
             first_name:this.firstname.value,
             last_name:this.lastname.value,
             phone:this.phone.value,
             address:this.address.value,
             address_note:this.address_note.value,
             avatar:this.avatar.value,
             bank_account_name:this.bankaccountname.value,
             bank_account_number:this.bankaccountnumber.value,
             bank_branch:this.bankbranch.value,
             bank_name:this.bankname.value,
             biography:this.biography.value,
             date_of_birth:this.state.birthday,
             gender:this.gender.value
        }
        axios.post('http://localhost:4000/api/updateinfluencer',requestuser)
            .then(res => {
                console.log(res);
                 if (res.data==="success") {
                     self.setState({success:"success"});
                    
                  }
            })
    }
    
    render() {
       
        return (
            <div className="row">
                <div className="col-md-10 col-lg-8 p-5 mx-auto">
                    <form onSubmit={this.updateclient}>
                        <h1 className="h3 my-3 font-weight-normal">Edit Profile</h1>
                        <div className="form-group row mt-5">
                            <label className="col-3 align-self-center mb-0">Email</label>
                            <input type="email"  name="email" className="form-control col-9  inputtxt" placeholder="Email address" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['email']:""}  ref={el => this.email = el} required />
                             
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">First Name</label>
                            <input type="text"  name="firstname" className="form-control col-9  inputtxt" placeholder="First Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['first_name']:""}  ref={el => this.firstname = el} required />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Last Name</label>
                            <input type="text"  name="lastname" className="form-control col-9  inputtxt" placeholder="Last Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['last_name']:""}  ref={el => this.lastname = el} required />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Gender</label>
                            <select name="gender" className="form-control col-9 inputtxt" style={{height:40}} defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['gender']:""} ref={el => this.gender = el} required>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Birthday</label>
                            <DatePicker  selected={this.state.birthday}  onChange={this.birthdayChange} className="form-control col-9 inputtxt" />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Phone</label>
                            <input type="text"  name="phone" className="form-control col-9  inputtxt" placeholder="Phone Number" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['phone']:""}  ref={el => this.phone = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Address</label>
                            <input type="text"  name="address" className="form-control col-9  inputtxt" placeholder="Address" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['address']:""}  ref={el => this.address= el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Address Note</label>
                            <input type="text"  name="addressnote" className="form-control col-9  inputtxt" placeholder="Address Note" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['address_note']:""}  ref={el => this.address_note = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Avatar</label>
                            <input type="text"  name="avatar" className="form-control col-9 inputtxt" placeholder="Avatar" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['avatar']:""}  ref={el => this.avatar = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Bank Name</label>
                            <input type="text"  name="bankname" className="form-control col-9  inputtxt" placeholder="Bank Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['bank_name']:""}  ref={el => this.bankname = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Bank Account Name</label>
                            <input type="text"  name="bankaccountname" className="form-control col-9  inputtxt" placeholder="Bank Account Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['bank_account_name']:""}  ref={el => this.bankaccountname = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Bank Account Number</label>
                            <input type="text"  name="bankaccountnumber" className="form-control col-9  inputtxt" placeholder="Bank Account Number" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['bank_account_number']:""}  ref={el => this.bankaccountnumber = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Bank Branch</label>
                            <input type="text"  name="bankbranch" className="form-control col-9  inputtxt" placeholder="Bank Branch" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['bank_branch']:""}  ref={el => this.bankbranch = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Biography</label>
                            <input type="text"  name="biography" className="form-control col-9  inputtxt" placeholder="Biography" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['biography']:""}  ref={el => this.biography = el} />
                        </div>
                        <button className="form-control col-3 inputtxt login-btn mt-5 p-0" type="submit" >Edit</button>
                       
                            {this.state.success? <div className="alert alert-primary mt-5" role="alert">Success</div> :""}
                    </form>
                    
                </div>
            </div>
        );
    }


}


export default Editinfluencer;
