import React, {Component} from 'react';
import {Route,Switch} from 'react-router';
import MainIndexPage from "./main_index";
import LoginPage from "./loginPage";
import ClientRegister from "./registerclient";
import InfluencerRegister from "./registerinfluencer";
import img from "../assets/images/bg-minimal.jpg";
import logo from '../assets/images/logo.png'
import '../../src/index.css'
var sectionStyle = {
     height: "100vh",
     backgroundImage: `url(${img})`
   };
class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        }
    }
    
    render() {

        return (
             <div className="main" style={sectionStyle}>
                <header>
                     <nav className="navbar">
                         <div className="container">
                            <a href="/" className="logo">
                                   <img  className="logo-dark default" src={logo} alt=""/>
                              </a>
                              <div className="collapse navbar-collapse" id="wexim">
                                   <div className="navbar-nav">
                                      <a href="/main/login" className="nav-link scroll active">Login</a>  
                                   </div>
                              </div>  
                         </div>
                    </nav>
               </header> 
               <div className="container-fluid" >
                    <Switch>
                         <Route exact path="/" component={MainIndexPage} />
                         <Route path="/main/login" component={LoginPage} />
                         <Route path="/main/clientregister" component={ClientRegister} />
                         <Route path="/main/influencerregister" component={InfluencerRegister} />
                    </Switch>
               </div> 
            </div>
        );
    }


}

export default MainPage;
