import React, {Component} from 'react';
import axios from 'axios';

class Editclient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initialuser:[],
            success:''
        }
    }
    componentWillMount(){
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getclient/'+ruser[0].id)
             .then(response => {
                 console.log(response.data);
                this.setState({initialuser: response.data});
                
             })
             .catch(function (error) {
                  console.log(error);
             })
    }
    updateclient= (e) => {
        var self=this;
        e.preventDefault();
        var requestuser={
             id:this.state.initialuser[0]["id"],
             user_id:this.state.initialuser[0]["user_id"],
             email:this.email.value,
             first_name:this.firstname.value,
             last_name:this.lastname.value,
             phone:this.phone.value,
             company_address:this.companyaddress.value,
             company_name:this.companyname.value,
             company_description:this.companydescription.value,
             company_photo:this.companyphoto.value,
             company_website:this.companywebsite.value,
        }
        axios.post('http://localhost:4000/api/updateclient',requestuser)
            .then(res => {
                console.log(res);
                 if (res.data==="success") {
                     self.setState({success:"success"});
                    
                  }
            })
    }
    
    render() {
       
        return (
            <div className="row">
                <div className="col-md-10 col-lg-8 p-5 mx-auto">
                    <form onSubmit={this.updateclient}>
                        <h1 className="h3 my-3 font-weight-normal">Edit Profile</h1>
                        <div className="form-group row mt-5">
                            <label className="col-3 align-self-center mb-0">Email</label>
                            <input type="email"  name="email" className="form-control col-9  inputtxt" placeholder="Email address" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['email']:""}  ref={el => this.email = el} required />
                             
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">First Name</label>
                            <input type="text"  name="firstname" className="form-control col-9  inputtxt" placeholder="First Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['first_name']:""}  ref={el => this.firstname = el} required />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Last Name</label>
                            <input type="text"  name="lastname" className="form-control col-9  inputtxt" placeholder="Last Name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['last_name']:""}  ref={el => this.lastname = el} required />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Phone</label>
                            <input type="text"  name="phone" className="form-control col-9  inputtxt" placeholder="Phone Number" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['phone']:""}  ref={el => this.phone = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Company name</label>
                            <input type="text"  name="companyname" className="form-control col-9  inputtxt" placeholder="Company name" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['company_name']:""}  ref={el => this.companyname= el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Company address</label>
                            <input type="text"  name="companyaddress" className="form-control col-9  inputtxt" placeholder="Company address" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['company_address']:""}  ref={el => this.companyaddress = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Company description</label>
                            <input type="text"  name="companydescription" className="form-control col-9 inputtxt" placeholder="Company description" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['company_description']:""}  ref={el => this.companydescription = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Company photo</label>
                            <input type="text"  name="companyphoto" className="form-control col-9  inputtxt" placeholder="Company photo" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['company_photo']:""}  ref={el => this.companyphoto = el} />
                        </div>
                        <div className="form-group row">
                            <label className="col-3 align-self-center mb-0">Company website</label>
                            <input type="text"  name="companywebsite" className="form-control col-9  inputtxt" placeholder="Company website" 
                            defaultValue={this.state.initialuser[0]? this.state.initialuser[0]['company_website']:""}  ref={el => this.companywebsite = el} />
                        </div>
                        <button className="form-control col-3 inputtxt login-btn mt-5 p-0" type="submit" >Edit</button>
                       
                            {this.state.success? <div className="alert alert-primary mt-5" role="alert">Success</div> :""}
                    </form>
                    
                </div>
            </div>
        );
    }


}


export default Editclient;
