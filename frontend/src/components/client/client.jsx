import React, {Component} from 'react';
import {Switch} from "react-router";
import {Route} from 'react-router';
import logo from '../../assets/images/logo.png';
import { Navbar, Nav,NavDropdown} from 'react-bootstrap';
import Editclient from "./editclient";
import Changepass from "../changepass";
import ClientCampaign from "./clientcampaign";
import ClientJobSchedule from "./cllientjobschedule";
import ClientInvoice from "./clientinvoice";
class Client extends Component {
    logout(event) {
        window.localStorage.removeItem('vinity');
        window.location.reload();
    }  
  
    render() {
        return (
            <div>
                 <header>
                     <div className="dashboard_navbar"> 
                         <div className="container d-flex">
                            <a href="/" className="logo">
                                   <img  className="logo-dark default" src={logo} alt=""/>
                            </a>
                            <Navbar collapseOnSelect expand="sm" variant="white" className="ml-auto">
                              <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                              <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="mr-auto">
                                        <Nav.Link href="/client/campaign">Campaign</Nav.Link>
                                        <Nav.Link href="/client/jobschedule">Job Schedule</Nav.Link>
                                        <Nav.Link href="/client/invoice">Invoice</Nav.Link>
                                        <NavDropdown title="Edit Profile" >
                                       
                                             <NavDropdown.Item href="/client/editprofile">Edit Profile</NavDropdown.Item>
                                             <NavDropdown.Item href="/client/changepass">Change Password</NavDropdown.Item>
                                        </NavDropdown>
                                        <Nav.Link onClick={(e) => {this.logout(e)}}>Logout</Nav.Link>
                                   </Nav>
                                   
                              </Navbar.Collapse>
                         </Navbar>
                         </div>
                     </div>
             </header> 

                <section>
                    <Switch>
                        <Route path="/client/editprofile" component={Editclient} />
                        <Route path="/client/changepass" component={Changepass} />
                        <Route path="/client/campaign" component={ClientCampaign} />
                        <Route path="/client/jobschedule" component={ClientJobSchedule} />
                        <Route path="/client/invoice" component={ClientInvoice} />
                    </Switch>
                </section>
            </div>
        );
    }
}

export default Client;
