import React, {Component} from 'react';
import axios from 'axios';
import { Modal, Button} from 'react-bootstrap';
import { MDBDataTable,MDBBtn } from 'mdbreact';
class ClientCampaign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initialuser:[],
            success:'',
            addmodal: false,
            updatemodal:false,
            productname:'',
            productcategory:'',
            productdescription:'',
            promotiongoal:'',
            promotionbudget:'',
            promotiondescription:'',
            tablelist:[],
            updatecampaign:[],
            campaigns:[]
        };
        this.openaddModal = this.openaddModal.bind(this);
        this.closeaddModal = this.closeaddModal.bind(this);
        this.openupdateModal = this.openupdateModal.bind(this);
        this.closeupdateModal = this.closeupdateModal.bind(this);
    }
    openaddModal() {
        this.setState({addmodal: true});
    }
     
    closeaddModal() {
        this.setState({addmodal: false});
    }
    openupdateModal() {
        this.setState({updatemodal: true});
    }
     
    closeupdateModal() {
        this.setState({updatemodal: false});
    }
    componentWillMount(){
        var self=this;
        var result = [];
        var actiontag;
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getclientcampaign/'+ruser[0].id)
             .then(response => {
                  console.log(response.data);
                  this.setState({campaigns: response.data});
                       response.data.map(function(campaign, i){
                           if (campaign.status===3) {
                                actiontag=<MDBBtn color="orange" className="removebtn" outline size="sm" onClick={(e) => {self.updatefirststatus(e, campaign.id)}}>Approve/Reject</MDBBtn>
                           } else {
                               actiontag="";
                           }
                            return result.push({"no":(i+1), "productname":campaign.product_name, 
                            "productcategory":campaign.product_category_id, 
                            "productdescription":campaign.product_description,
                            "promotiongoal":campaign.promotion_goal_id,
                            "promotionbudget":campaign.promotion_budget,
                            "promotiondescription":campaign.promotion_description,
                            "status":self.getstatusname(campaign.status),
                            "createddate":self.timeConverter(campaign.created_date),
                            "modifieddate":self.timeConverter(campaign.modified_date),
                            "action":actiontag
                        
                       });
                    });
                this.setState({tablelist: result});
            }) 
            
    }
    updatefirststatus(e,id) {
        this.setState({updatemodal: true});
        var updatecampaign=this.state.campaigns.find((e) => e.id === id);
        this.setState({updatecampaign:updatecampaign});
        console.log(updatecampaign);
        
    }
    addcampaign=(e) => {
        var self=this;
        e.preventDefault();
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        var requestcampaign={
             clientid:ruser[0].id,
             productname:this.state.productname,
             productcategory:this.state.productcategory,
             productdescription:this.state.productdescription,
             promotiongoal:this.state.promotiongoal,
             promotionbudget:this.state.promotionbudget,
             promotiondescription:this.state.promotiondescription,

        }
        axios.post('http://localhost:4000/api/addcampaign', requestcampaign)
            .then(res => {
                console.log(res.data);
                 if (res.data==="success") {
                     self.closeaddModal();
                     window.location.reload();
                 }
            })
   }
   approve(e,id) {
        var req={
            status:4,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    reject(e,id) {
        var req={
            status:10,
            id:id
        }
        axios.post('http://localhost:4000/api/updatecampaignstatus/',req)
        .then(response => {
            if (response.data==="success") {
                window.location.reload();
            }

        });
    }
    onChange = (e) => {
          
        this.setState({ [e.target.name]: e.target.value });

    }     
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
    getstatusname(id){
        if (id===1) {
            return "Waiting VInity's approval";
        } else if (id===2) {
            return "Waiting VInity's recommendation";
        } else if (id===3) {
            return "Waiting Client's feedback";
        }  else if (id===4) {
            return "Waiting Influencer's feedback";
        } else if (id===5) {
            return "Waiting for payments";
        } else if (id===6) {
            return "Payment accepted";
        } else if (id===7) {
            return "Marketing Execution";
        } else if (id===8) {
            return "Done";
        } else if (id===9) {
            return "Done";
        } else if (id===10) {
            return "Client Rejected";
        }
    
    }
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Product Name',
                field: 'productname',
                width: 250
              },
              {
                 label: 'Product Category',
                 field: 'productcategory',
                 width: 250
               },
              {
                label: 'Product Description',
                field: 'productdescription',
                width: 250
              },
              {
                 label: 'Promotion Goal',
                 field: 'promotiongoal',
                 width: 250
               },
               {
                 label: 'Promotion Budget',
                 field: 'promotionbudget',
                 width: 250
               },
               {
                 label: 'Promotion Description',
                 field: 'promotiondescription',
                 width: 200
               },
               {
                label: 'Status',
                field: 'status',
                width: 200
              },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              {
                label: 'Action',
                field: 'action',
                width: 200
              },
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">Campaigns</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                        <button className="form-control col-3 inputtxt login-btn mt-5 p-0" type="submit" onClick={this.openaddModal} >Request</button>
                        
                </div>
                <div>
                    <Modal show={this.state.addmodal} onHide={this.closeaddModal} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Request Campaign</Modal.Title>
                        </Modal.Header>
                        <form onSubmit={this.addcampaign} className="p-3">
                            <Modal.Body>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Name</label>
                                    <input type="text"  name="productname" className="form-control col-9 inputtxt" placeholder="Product Name" onChange={this.onChange} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Categoryid</label>
                                    <input type="text"  name="productcategory" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Description</label>
                                    <input type="text"  name="productdescription" className="form-control col-9 inputtxt" placeholder="Product Description" onChange={this.onChange} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Goal</label>
                                    <input type="text"  name="promotiongoal" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange}  required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Budget</label>
                                    <input type="text"  name="promotionbudget" className="form-control col-9 inputtxt" placeholder="Type Number" onChange={this.onChange} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Description</label>
                                    <input type="text"  name="promotiondescription" className="form-control col-9 inputtxt" placeholder="Promotion Description" onChange={this.onChange}  required />
                                </div>
                                    
                            </Modal.Body> 
                        <Modal.Footer>
                            <Button variant="primary"  type="submit">Request</Button>
                            <Button variant="light"  onClick={this.closeaddModal}>Close</Button>
                            
                        </Modal.Footer>  
                        </form>;                 
                    </Modal>                    
                    </div>
                    <div>
                    <Modal show={this.state.updatemodal} onHide={this.closeupdateModal} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Client Campaign</Modal.Title>
                        </Modal.Header>
                        
                            <Modal.Body>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Name</label>
                                    <input type="text"  name="productname" className="form-control col-9 inputtxt" placeholder="Product Name" defaultValue={this.state.updatecampaign['product_name']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Categoryid</label>
                                    <input type="text"  name="productcategory" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['product_category_id']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Product Description</label>
                                    <input type="text"  name="productdescription" className="form-control col-9 inputtxt" placeholder="Product Description" defaultValue={this.state.updatecampaign['product_description']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Goalid</label>
                                    <input type="text"  name="promotiongoal" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['promotion_goal_id']}  required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Budget</label>
                                    <input type="text"  name="promotionbudget" className="form-control col-9 inputtxt" placeholder="Type Number" defaultValue={this.state.updatecampaign['promotion_budget']} required />
                                </div>
                                <div className="form-group row ">
                                    <label className="col-3 align-self-center mb-0">Promotion Description</label>
                                    <input type="text"  name="promotiondescription" className="form-control col-9 inputtxt" placeholder="Promotion Description" defaultValue={this.state.updatecampaign['promotion_description']}  required />
                                </div>
                                    
                            </Modal.Body> 
                        <Modal.Footer>
                            <Button variant="primary"  onClick={(e) => {this.approve(e, this.state.updatecampaign['id'])}}>Approve</Button>
                            <Button variant="primary"  onClick={(e) => {this.reject(e, this.state.updatecampaign['id'])}}>Reject</Button>
                            <Button variant="light"  onClick={this.closeupdateModal}>Close</Button>
                            
                        </Modal.Footer>  
                                       
                    </Modal>                    
                    </div>

            </div>
            
        );
    }


}


export default ClientCampaign;
