import React, {Component} from 'react';
import axios from 'axios';

import { MDBDataTable} from 'mdbreact';
class ClientInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            invoices:[],
            tablelist:[]
        };
       
    }
   
    componentWillMount(){
        var self=this;
        var result = [];
        var user=localStorage.getItem('vinity');
        var ruser=JSON.parse(user);
        axios.get('http://localhost:4000/api/getinvoice/'+ruser[0].id)
             .then(response => {
                  console.log(response.data);
                  this.setState({invoices: response.data});
                       response.data.map(function(invoice, i){
                            return result.push({"no":(i+1), "campaignid":invoice.campaign_id, 
                            "status":invoice.status,
                            "amount":invoice.amount,
                            "createddate":self.timeConverter(invoice.created_date),
                            "modifieddate":self.timeConverter(invoice.modified_date),
                           
                         });
                                                
                       });
                  this.setState({tablelist: result});
             
        })   
    }
    
   
    timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp);
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        month = month.toString().length === 1 ? "0" + month : month;
        var date = a.getDate();
        date = date.toString().length === 1 ? "0" + date : date;
        var time = month + '/' + date + '/' + year;

        return time;
    }
   
    render() {
        const data = {
            columns: [
              {
                label: 'No',
                field: 'no',
                width: 100
              },
              {
                label: 'Campaign Id',
                field: 'campaignid',
                width: 250
              },
              {
                label: 'Status',
                field: 'status',
                width: 250
              },
              {
                 label: 'Amount',
                 field: 'amount',
                 width: 250
               },
              {
                label: 'Created Date',
                field: 'createddate',
                width: 200
              },
              {
                label: 'Modified Date',
                field: 'modifieddate',
                width: 200
              },
              
            ],
            rows: this.state.tablelist
       };
        return (
            <div className="row">
                <div className="col-md-12 col-lg-10 p-5 mx-auto">
                    
                        <h1 className="h3 my-3 font-weight-normal">Invoices</h1>
                        <MDBDataTable
                              sortable={false}
                              responsive={true}
                              data={data}
                         />
                         
                </div>
            </div>
            
        );
    }


}


export default ClientInvoice;
